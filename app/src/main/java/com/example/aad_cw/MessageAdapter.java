package com.example.aad_cw;

import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MessageViewHolder>
{
    private List<Messages> ChatMessageList;

    private FirebaseAuth mAuth;
    private DatabaseReference UsersRef;

    public  MessageAdapter (List<Messages> ChatMessageList)
    {
        this.ChatMessageList = ChatMessageList;
    }

    // link SenderMessage, ReceiverMessage with sender_message_content,receiver_message_content
    public class MessageViewHolder extends RecyclerView.ViewHolder
    {
        public TextView SenderMessage, ReceiverMessage;

        public MessageViewHolder(@NonNull View itemView)
        {
            super(itemView);

            SenderMessage = (TextView) itemView.findViewById(R.id.sender_message_content);
            ReceiverMessage = (TextView) itemView.findViewById(R.id.receiver_message_content);
        }
    }

    @NonNull
    @Override
    public MessageViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType)
    {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.chat_message_layout, viewGroup, false);

        mAuth = FirebaseAuth.getInstance();

        return new MessageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MessageViewHolder holder, int position)
    {
        String MessageSenderID = mAuth.getCurrentUser().getUid();
        Messages messages = ChatMessageList.get(position);

        String fromUserID = messages.getFrom();
        String fromMessageType = messages.getType();



        UsersRef = FirebaseDatabase.getInstance().getReference().child("Users").child(fromUserID);
        UsersRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {

            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });

        if (fromMessageType.equals("Text"))
        {
            holder.ReceiverMessage.setVisibility(View.INVISIBLE);

            if (fromUserID.equals(MessageSenderID))
            {
                holder.SenderMessage.setBackgroundResource(R.drawable.sender_message_layout);
                holder.SenderMessage.setText(messages.getMessage());
            }
            else
            {
                holder.SenderMessage.setVisibility(View.INVISIBLE);
                holder.ReceiverMessage.setVisibility(View.VISIBLE);

                holder.ReceiverMessage.setBackgroundResource(R.drawable.receiver_message_layout);
                holder.ReceiverMessage.setText(messages.getMessage());
            }
        }
    }

    @Override
    public int getItemCount()
    {
        //count how many messages
        return ChatMessageList.size();
    }


}
