package com.example.aad_cw;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PrivateChatActivity extends AppCompatActivity
{
    private Toolbar toolbar;
    private ImageButton SendMessageButton;
    private EditText editMessage;

    private String ReceiverID, ReceiverName, ReceiverImage, SenderID;

    private FirebaseAuth mAuth;
    private DatabaseReference ChatMessageRef;

    private final List<Messages> messagesList = new ArrayList<>();
    private LinearLayoutManager linearLayoutManager;
    private MessageAdapter messageAdapter;

    private RecyclerView PrivateChatMessageList;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_private_chat);



        mAuth = FirebaseAuth.getInstance();
        ChatMessageRef = FirebaseDatabase.getInstance().getReference();
        SenderID = mAuth.getCurrentUser().getUid();

        ReceiverID = getIntent().getExtras().get("receiver_user_id").toString();
        ReceiverName = getIntent().getExtras().get("receiver_user_name").toString();
        ReceiverImage = getIntent().getExtras().get("receiver_user_image").toString();


        Toast.makeText(this, ReceiverID + "\n" + ReceiverName, Toast.LENGTH_SHORT).show();

        toolbar = (Toolbar) findViewById(R.id.private_chat_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(ReceiverName);

        SendMessageButton = (ImageButton) findViewById(R.id.send_message_button);
        editMessage = (EditText) findViewById(R.id.private_chat_message);

        SendMessageButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                SendMessage();
            }
        });

        messageAdapter = new MessageAdapter(messagesList);
        PrivateChatMessageList = (RecyclerView) findViewById(R.id.private_chat_message_list);
        linearLayoutManager = new LinearLayoutManager(this);
        PrivateChatMessageList.setLayoutManager(linearLayoutManager);
        PrivateChatMessageList.setAdapter(messageAdapter);

        SensorManagerHelper sensorHelper = new SensorManagerHelper(this);
        sensorHelper.setOnShakeListener(new SensorManagerHelper.OnShakeListener()
        {
            @Override
            public void onShake()
            {
                SendMessage();
            }
        });
    }

    @Override
    protected void onStart()
    {
        super.onStart();

        ChatMessageRef.child("ChatMessages").child(SenderID).child(ReceiverID)
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s)
                    {
                        Messages messages = dataSnapshot.getValue(Messages.class);

                        messagesList.add(messages);

                        messageAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s)
                    {

                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot)
                    {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {


                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError)
                    {

                    }
                });
    }

    private void SendMessage()
    {
        final String message =  editMessage.getText().toString();

        if (TextUtils.isEmpty(message))
        {
            Toast.makeText(this, "Please type something first", Toast.LENGTH_SHORT).show();
        }
        else
        {
            //define ChatMessage storage format
            String SenderRef = "ChatMessages/" + SenderID + "/" + ReceiverID;
            String ReceiverRef = "ChatMessages/" + ReceiverID + "/" + SenderID;

            DatabaseReference  UserMessageKeyRef = ChatMessageRef.child("ChatMessages").child(SenderID).child(ReceiverID).push();

            String MessagePushID = UserMessageKeyRef.getKey();

            Map MessageText = new HashMap();
            MessageText.put("message", message);
            MessageText.put("type", "Text");
            MessageText.put("from", SenderID);

            Map MessageContent = new HashMap();
            MessageContent.put(SenderRef + "/" + MessagePushID, MessageText);
            MessageContent.put(ReceiverRef + "/" + MessagePushID, MessageText);

            ChatMessageRef.updateChildren(MessageContent).addOnCompleteListener(new OnCompleteListener()
            {
                @Override
                public void onComplete(@NonNull Task task)
                {
                    if (task.isSuccessful())
                    {
                        Toast.makeText(PrivateChatActivity.this, "Message Sent", Toast.LENGTH_SHORT).show();
                        editMessage.setText("");
                    }
                    else
                    {
                        Toast.makeText(PrivateChatActivity.this, "Error", Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }
    }
}
