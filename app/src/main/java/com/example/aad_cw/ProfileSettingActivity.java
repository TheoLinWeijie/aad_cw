package com.example.aad_cw;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileSettingActivity extends AppCompatActivity
{
    private EditText editUserName, editWhatsUp;
    private Button Save;
    private CircleImageView UserImageView;
    private Toolbar toolbar;

    private FirebaseAuth mAuth;
    private DatabaseReference myRef;

    private String currentUserID;

    private static final int GalleryPick = 1;

    private StorageReference myStorageRef;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_setting);

        toolbar = (Toolbar) findViewById(R.id.profile_setting_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Profile Setting");

        editUserName = (EditText) findViewById(R.id.edit_user_name);
        editWhatsUp = (EditText) findViewById(R.id.edit_what_up);
        Save = (Button) findViewById(R.id.save_change);
        UserImageView = (CircleImageView) findViewById(R.id.profile_image);

        mAuth = FirebaseAuth.getInstance();
        myRef = FirebaseDatabase.getInstance().getReference();
        myStorageRef = FirebaseStorage.getInstance().getReference().child("image");
        currentUserID = mAuth.getCurrentUser().getUid();

        
        Save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) 
            {
                SaveChange();
            }
        });

        UserImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Intent GalleryIntent = new Intent();
                GalleryIntent.setAction(Intent.ACTION_GET_CONTENT);
                GalleryIntent.setType("image/*");
                startActivityForResult(GalleryIntent, GalleryPick);
            }
        });

        showUserInfo();
        
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GalleryPick && resultCode == RESULT_OK && data != null )
        {
            Uri ImageUri = data.getData();
            CropImage.activity()
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setAspectRatio(1,1)
                    .start(this);
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE)
        {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if (resultCode == RESULT_OK)
            {
                Uri resultUri = result.getUri();

                StorageReference ImagePath = myStorageRef.child(currentUserID);
                ImagePath.putFile(resultUri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task)
                    {
                        if (task.isSuccessful())
                        {
                            final String downloadUrl = task.getResult().getDownloadUrl().toString();

                            myRef.child("Users").child(currentUserID).child("image").setValue(downloadUrl)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task)
                                         {
                                            if (task.isSuccessful())
                                            {
                                                Toast.makeText(ProfileSettingActivity.this, "Image upload to database", Toast.LENGTH_SHORT).show();
                                                GoToMainActivity();
                                            }
                                            else
                                            {
                                                Toast.makeText(ProfileSettingActivity.this, "Error", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });

                            Toast.makeText(ProfileSettingActivity.this, "Waiting for Uploading your image", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            Toast.makeText(ProfileSettingActivity.this, "Error", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        }
    }

    private void SaveChange()
    {
        String setUserName = editUserName.getText().toString();
        String setWhatsUp = editWhatsUp.getText().toString();

        if (TextUtils.isEmpty(setUserName))
        {
            Toast.makeText(this, "Please type for your user name", Toast.LENGTH_SHORT).show();
        }
        if (TextUtils.isEmpty(setWhatsUp))
        {
            Toast.makeText(this, "Please type for your What's Up", Toast.LENGTH_SHORT).show();
        }
        else
        {
            HashMap<String, Object> UserInfoMap = new HashMap<>();
            UserInfoMap.put("uid", currentUserID);
            UserInfoMap.put("name", setUserName);
            UserInfoMap.put("whatsup", setWhatsUp);

            myRef.child("Users").child(currentUserID).updateChildren(UserInfoMap)
                    .addOnCompleteListener(new OnCompleteListener<Void>()
                    {
                        @Override
                        public void onComplete(@NonNull Task<Void> task)
                        {
                            if (task.isSuccessful())
                            {
                                GoToMainActivity();
                                Toast.makeText(ProfileSettingActivity.this, "Your Information are saved ", Toast.LENGTH_SHORT).show();
                            }
                            else
                            {
                                Toast.makeText(ProfileSettingActivity.this, "Error, Please check the UserName or What's Up", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }
    }

    private void showUserInfo()
    {
        myRef.child("Users").child(currentUserID)
                .addValueEventListener(new ValueEventListener()
                {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot)
                    {
                        if ((dataSnapshot.exists()) && (dataSnapshot.hasChild("name")) && (dataSnapshot.hasChild("image")))
                        {
                            String showUserName = dataSnapshot.child("name").getValue().toString();
                            String showWhatsUp = dataSnapshot.child("whatsup").getValue().toString();
                            String showUserIamge = dataSnapshot.child("image").getValue().toString();

                            editUserName.setText(showUserName);
                            editWhatsUp.setText(showWhatsUp);
                            Picasso.get().load(showUserIamge).into(UserImageView);


                        }
                        if ((dataSnapshot.exists()) && (dataSnapshot.hasChild("name")))
                        {
                            String showUserName = dataSnapshot.child("name").getValue().toString();
                            String showWhatsUp = dataSnapshot.child("whatsup").getValue().toString();


                            editUserName.setText(showUserName);
                            editWhatsUp.setText(showWhatsUp);

                        }
                        else
                        {
                            Toast.makeText(ProfileSettingActivity.this, "You haven't set your information.", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError)
                    {

                    }
                });
    }

    private void GoToMainActivity()
    {
        Intent mainIntent = new Intent(ProfileSettingActivity.this,MainActivity.class);
        startActivity(mainIntent);
    }
}
