package com.example.aad_cw;

public class Contacts
{
    public String image, name, uid, whatsup;

    public  Contacts()
    {

    }

    public Contacts(String image, String name, String uid, String whatsup) {
        this.image = image;
        this.name = name;
        this.uid = uid;
        this.whatsup = whatsup;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getWhatsup() {
        return whatsup;
    }

    public void setWhatsup(String whatsup) {
        this.whatsup = whatsup;
    }
}
