package com.example.aad_cw;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class AddContactActivity extends AppCompatActivity
{
    private Toolbar toolbar;
    private RecyclerView AddContactRecyclerView;

    private DatabaseReference UsersRef;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);

        toolbar = (Toolbar) findViewById(R.id.add_contact_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Add Contact");

        AddContactRecyclerView = (RecyclerView) findViewById(R.id.add_contact_recycle_view);
        AddContactRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        UsersRef = FirebaseDatabase.getInstance().getReference().child("Users");
    }

    //Use RecyclerView Show User Info
    @Override
    protected void onStart()
    {
        super.onStart();

        FirebaseRecyclerOptions<Contacts> options = new FirebaseRecyclerOptions.Builder<Contacts>()
                .setQuery(UsersRef, Contacts.class)
                .build();

        FirebaseRecyclerAdapter<Contacts, AddContactViewHolder> adapter = new FirebaseRecyclerAdapter<Contacts, AddContactViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull AddContactViewHolder addContactViewHolder, final int i, @NonNull Contacts contacts)
            {
                addContactViewHolder.username.setText(contacts.getName());
                addContactViewHolder.userwhatsup.setText(contacts.getWhatsup());
                Picasso.get().load(contacts.getImage()).into(addContactViewHolder.userimageview);

                //itemView button
                addContactViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v)
                    {
                        String click_user_id = getRef(i).getKey();
                        Intent AddContactProfileIntent = new Intent(AddContactActivity.this, AddContactProfileActivity.class);
                        AddContactProfileIntent.putExtra("click_user_id", click_user_id);
                        startActivity(AddContactProfileIntent);
                    }
                });
            }

            @NonNull
            @Override
            public AddContactViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
            {
                View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.add_contact_user_display, viewGroup, false);
                AddContactViewHolder viewHolder = new AddContactViewHolder(view);
                return viewHolder;
            }
        };

        AddContactRecyclerView.setAdapter(adapter);

        adapter.startListening();
    }

    //set all info save to itemView
    public static class AddContactViewHolder extends RecyclerView.ViewHolder
    {
        TextView username, userwhatsup;
        CircleImageView userimageview;

        public AddContactViewHolder(@NonNull View itemView)
        {
            super(itemView);

            username = itemView.findViewById(R.id.add_contact_username);
            userwhatsup = itemView.findViewById(R.id.add_contact_whatsup);
            userimageview = itemView.findViewById(R.id.add_contact_user_image);
        }
    }


}
