package com.example.aad_cw;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * A simple {@link Fragment} subclass.
 */
public class ChatFragment extends Fragment
{
    private View PrivateChatView;
    private RecyclerView PrivateChatList;

    private String currentUserID = "";

    private DatabaseReference PrivateChatRef, UsersRef;
    private FirebaseAuth mAuth;

    public ChatFragment()
    {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        PrivateChatView = inflater.inflate(R.layout.fragment_chat, container, false);

        mAuth = FirebaseAuth.getInstance();
        FirebaseUser mFirebaseUser = mAuth.getCurrentUser();
        if(mFirebaseUser != null) {
            currentUserID = mFirebaseUser.getUid(); //Do what you need to do with the id
        }
        PrivateChatRef = FirebaseDatabase.getInstance().getReference().child("Contact").child(currentUserID);
        UsersRef = FirebaseDatabase.getInstance().getReference().child("Users");

        PrivateChatList = (RecyclerView) PrivateChatView.findViewById(R.id.chat_recycle_list);
        PrivateChatList.setLayoutManager(new LinearLayoutManager(getContext()));

        return PrivateChatView;
    }

    @Override
    public void onStart()
    {
        super.onStart();

        FirebaseRecyclerOptions<Contacts> options = new FirebaseRecyclerOptions.Builder<Contacts>()
                .setQuery(PrivateChatRef, Contacts.class)
                .build();

        FirebaseRecyclerAdapter<Contacts, PrivateChatViewHolder> adapter = new FirebaseRecyclerAdapter<Contacts, PrivateChatViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull final PrivateChatViewHolder privateChatViewHolder, int i, @NonNull Contacts contacts)
            {
                final String UserIDs = getRef(i).getKey();

                UsersRef.child(UserIDs).addValueEventListener(new ValueEventListener()
                {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot)
                    {
                        if (dataSnapshot.hasChild("image"))
                        {
                            final String ReceiverImage = dataSnapshot.child("image").getValue().toString();
                            final String ReceiverName = dataSnapshot.child("name").getValue().toString();
                            String ReceiverWhatsup = dataSnapshot.child("whatsup").getValue().toString();

                            Picasso.get().load(ReceiverImage).placeholder(R.drawable.profileicon).into(privateChatViewHolder.userimageview);
                            privateChatViewHolder.username.setText(ReceiverName);
                            privateChatViewHolder.userwhatsup.setText("Last Message");

                            //click itemView
                            privateChatViewHolder.itemView.setOnClickListener(new View.OnClickListener()
                            {
                                @Override
                                public void onClick(View v)
                                {
                                    Intent PrivateChatIntent = new Intent(getContext(),PrivateChatActivity.class);
                                    PrivateChatIntent.putExtra("receiver_user_id", UserIDs);
                                    PrivateChatIntent.putExtra("receiver_user_name", ReceiverName);
                                    PrivateChatIntent.putExtra("receiver_user_image", ReceiverImage);
                                    startActivity(PrivateChatIntent);
                                }
                            });
                        }
                        else
                        {
                            final String ReceiverName = dataSnapshot.child("name").getValue().toString();
                            String ReceiverWhatsup = dataSnapshot.child("whatsup").getValue().toString();

                            privateChatViewHolder.username.setText(ReceiverName);
                            privateChatViewHolder.userwhatsup.setText("Last Message");

                            //click itemView
                            privateChatViewHolder.itemView.setOnClickListener(new View.OnClickListener()
                            {
                                @Override
                                public void onClick(View v)
                                {
                                    Intent PrivateChatIntent = new Intent(getContext(),PrivateChatActivity.class);
                                    PrivateChatIntent.putExtra("receiver_user_id",UserIDs);
                                    PrivateChatIntent.putExtra("receiver_user_name",ReceiverName);
                                    startActivity(PrivateChatIntent);
                                }
                            });
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError)
                    {

                    }
                });
            }

            @NonNull
            @Override
            public PrivateChatViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType)
            {
                View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.add_contact_user_display, viewGroup, false);
                return  new PrivateChatViewHolder(view);
            }
        };

        PrivateChatList.setAdapter(adapter);
        adapter.startListening();

    }

    public static class  PrivateChatViewHolder extends RecyclerView.ViewHolder
    {
        TextView username, userwhatsup;
        CircleImageView userimageview;

        public PrivateChatViewHolder(@NonNull View itemView)
        {
            super(itemView);

            username = itemView.findViewById(R.id.add_contact_username);
            userwhatsup = itemView.findViewById(R.id.add_contact_whatsup);
            userimageview = itemView.findViewById(R.id.add_contact_user_image);

        }
    }
}
