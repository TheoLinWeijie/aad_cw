package com.example.aad_cw;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * A simple {@link Fragment} subclass.
 */
public class FriendFragment extends Fragment
{
    private View FriendView;
    private RecyclerView FriendRecycleList;

    private DatabaseReference ContactRef, UsersRef;
    private FirebaseAuth mAuth;

    private String CurrentUserID = "1111";

    public FriendFragment()
    {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        FriendView = inflater.inflate(R.layout.fragment_friend, container, false);

        FriendRecycleList = (RecyclerView) FriendView.findViewById(R.id.friend_recycle_list);
        FriendRecycleList.setLayoutManager(new LinearLayoutManager(getContext()));

        mAuth = FirebaseAuth.getInstance();

        FirebaseUser mFirebaseUser = mAuth.getCurrentUser();
        if(mFirebaseUser != null) {
            CurrentUserID = mFirebaseUser.getUid(); //Do what you need to do with the id
        }

        ContactRef = FirebaseDatabase.getInstance().getReference().child("Contact").child(CurrentUserID);
        UsersRef = FirebaseDatabase.getInstance().getReference().child("Users");



        return  FriendView;
    }

    @Override
    public void onStart()
    {
        super.onStart();

        FirebaseRecyclerOptions options = new FirebaseRecyclerOptions.Builder<Contacts>()
                .setQuery(ContactRef, Contacts.class)
                .build();


        FirebaseRecyclerAdapter<Contacts,FriendViewHolder> adapter = new FirebaseRecyclerAdapter<Contacts, FriendViewHolder>(options)
        {
            @Override
            protected void onBindViewHolder(@NonNull final FriendViewHolder friendViewHolder, int i, @NonNull Contacts contacts)
            {
                final String UserIDs = getRef(i).getKey();

                UsersRef.child(UserIDs).addValueEventListener(new ValueEventListener()
                {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot)
                    {
                        if (dataSnapshot.hasChild("image"))
                        {
                            String FriendImage = dataSnapshot.child("image").getValue().toString();
                            String FriendName = dataSnapshot.child("name").getValue().toString();
                            String FriendWhatsup = dataSnapshot.child("whatsup").getValue().toString();

                            Picasso.get().load(FriendImage).placeholder(R.drawable.profileicon).into(friendViewHolder.userimageview);
                            friendViewHolder.username.setText(FriendName);
                            friendViewHolder.userwhatsup.setText(FriendWhatsup);

                        }
                        else
                        {
                            String FriendName = dataSnapshot.child("name").getValue().toString();
                            String FriendWhatsup = dataSnapshot.child("whatsup").getValue().toString();

                            friendViewHolder.username.setText(FriendName);
                            friendViewHolder.userwhatsup.setText(FriendWhatsup);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError)
                    {

                    }
                });
            }

            @NonNull
            @Override
            public FriendViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType)
            {
                View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.add_contact_user_display, viewGroup, false);
                FriendViewHolder viewHolder = new FriendViewHolder(view);
                return viewHolder;
            }
        };
        FriendRecycleList.setAdapter(adapter);

        adapter.startListening();

    }

    //call the object from layout save to username, userwhatsup, userimageview
    public static class FriendViewHolder extends RecyclerView.ViewHolder
    {
        TextView username, userwhatsup;
        CircleImageView userimageview;

        public FriendViewHolder(@NonNull View itemView)
        {
            super(itemView);

            username = itemView.findViewById(R.id.add_contact_username);
            userwhatsup = itemView.findViewById(R.id.add_contact_whatsup);
            userimageview = itemView.findViewById(R.id.add_contact_user_image);
        }
    }
}
