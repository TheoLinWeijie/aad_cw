package com.example.aad_cw;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class AddContactProfileActivity extends AppCompatActivity
{
    private Toolbar toolbar;
    private String senderUID, receiverUID, Current_State;

    private CircleImageView AddContactProfileImage;
    private TextView AddContactProfileUserName, AddContactProfileWhatsUp;
    private Button AddContactProfileFriendRequest, AddContactProfileRefuseRequest;

    private DatabaseReference UsersRef, FriendRequestRef, ContactRef;
    private FirebaseAuth mAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact_profile);

        toolbar = (Toolbar) findViewById(R.id.add_contact_profile_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Add Contact Profile");

        UsersRef = FirebaseDatabase.getInstance().getReference().child("Users");
        FriendRequestRef = FirebaseDatabase.getInstance().getReference().child("Friend Request");
        ContactRef = FirebaseDatabase.getInstance().getReference().child("Contact");
        mAuth = FirebaseAuth.getInstance();

        senderUID = mAuth.getCurrentUser().getUid();
        receiverUID = getIntent().getExtras().get("click_user_id").toString();
        Current_State = "unfriend";

        AddContactProfileImage = (CircleImageView) findViewById(R.id.add_contact_profile_image);
        AddContactProfileUserName = (TextView) findViewById(R.id.add_contact_profile_username);
        AddContactProfileWhatsUp = (TextView) findViewById(R.id.add_contact_profile_whatsup);
        AddContactProfileFriendRequest = (Button) findViewById(R.id.add_contact_profile_friend_request);
        AddContactProfileRefuseRequest = (Button) findViewById(R.id.add_contact_profile_refuse_request);

        ShowReceiverInfo();

}

    private void ShowReceiverInfo()
    {
        UsersRef.child(receiverUID).addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                if ((dataSnapshot.exists()) && dataSnapshot.hasChild("image"))
                {
                    String UserIamge = dataSnapshot.child("image").getValue().toString();
                    String UserName = dataSnapshot.child("name").getValue().toString();
                    String UserWhatsup = dataSnapshot.child("whatsup").getValue().toString();

                    Picasso.get().load(UserIamge).placeholder(R.drawable.profileicon).into(AddContactProfileImage);
                    AddContactProfileUserName.setText(UserName);
                    AddContactProfileWhatsUp.setText(UserWhatsup);

                    FriendRequest();
                }
                else
                {
                    String UserName = dataSnapshot.child("name").getValue().toString();
                    String UserWhatsup = dataSnapshot.child("whatsup").getValue().toString();

                    AddContactProfileUserName.setText(UserName);
                    AddContactProfileWhatsUp.setText(UserWhatsup);

                    FriendRequest();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });
    }

    private void FriendRequest()
    {
        FriendRequestRef.child(senderUID).addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                if (dataSnapshot.hasChild(receiverUID))
                {
                    String request_type = dataSnapshot.child(receiverUID).child("request_type").getValue().toString();

                    if (request_type.equals("sent"))
                    {
                        Current_State = "request_sent";
                        AddContactProfileFriendRequest.setText("Cancel The Request");
                    }
                    else if (request_type.equals("received"))
                    {
                        Current_State = "request_received";
                        AddContactProfileFriendRequest.setText("Accept The Request");
                        AddContactProfileRefuseRequest.setVisibility(View.VISIBLE);
                        AddContactProfileRefuseRequest.setEnabled(true);

                        AddContactProfileRefuseRequest.setOnClickListener(new View.OnClickListener()
                        {
                            @Override
                            public void onClick(View v)
                            {
                                CancelFriendRequest();
                            }
                        });
                    }
                }
                else
                {
                    ContactRef.child(senderUID).addListenerForSingleValueEvent(new ValueEventListener()
                    {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot)
                        {
                            if (dataSnapshot.hasChild(receiverUID))
                            {
                                Current_State = "friend";
                                AddContactProfileFriendRequest.setText("Delete Friend");
                            }

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError)
                        {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        //Determine whether the user is himself or another user
        if (!senderUID.equals(receiverUID))
        {
            AddContactProfileFriendRequest.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    AddContactProfileFriendRequest.setEnabled(false);

                    if (Current_State.equals("unfriend"))
                    {
                        SendFriendRequestToReceiver();
                    }
                    if (Current_State.equals("request_sent"))
                    {
                        CancelFriendRequest();
                    }
                    if (Current_State.equals("request_received"))
                    {
                        AcceptTheRequest();
                    }
                    if (Current_State.equals("friend"))
                    {
                        DeleteFriend();
                    }
                }
            });
        }
        else
        {
            AddContactProfileFriendRequest.setVisibility(View.INVISIBLE);
        }
    }

    private void DeleteFriend()
    {
        ContactRef.child(senderUID).child(receiverUID).removeValue()
                .addOnCompleteListener(new OnCompleteListener<Void>()
                {
                    @Override
                    public void onComplete(@NonNull Task<Void> task)
                    {
                        if (task.isSuccessful())
                        {
                            ContactRef.child(receiverUID).child(senderUID).removeValue()
                                    .addOnCompleteListener(new OnCompleteListener<Void>()
                                    {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task)
                                        {
                                            if (task.isSuccessful())
                                            {
                                                AddContactProfileFriendRequest.setText("Friend Request");
                                                AddContactProfileFriendRequest.setEnabled(true);
                                                Current_State = "unfriend";

                                                AddContactProfileRefuseRequest.setVisibility(View.INVISIBLE);
                                                AddContactProfileRefuseRequest.setEnabled(false);

                                            }
                                        }
                                    });
                        }
                    }
                });
    }

    private void AcceptTheRequest()
    {
        ContactRef.child(senderUID).child(receiverUID).child("Contact").setValue("Saved")
                .addOnCompleteListener(new OnCompleteListener<Void>()
                {
                    @Override
                    public void onComplete(@NonNull Task<Void> task)
                    {
                        if (task.isSuccessful())
                        {
                            ContactRef.child(receiverUID).child(senderUID).child("Contact").setValue("Saved")
                                    .addOnCompleteListener(new OnCompleteListener<Void>()
                                    {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task)
                                        {
                                            if (task.isSuccessful())
                                            {
                                                FriendRequestRef.child(senderUID).child(receiverUID).removeValue()
                                                        .addOnCompleteListener(new OnCompleteListener<Void>()
                                                        {
                                                            @Override
                                                            public void onComplete(@NonNull Task<Void> task)
                                                            {
                                                                FriendRequestRef.child(receiverUID).child(senderUID).removeValue()
                                                                        .addOnCompleteListener(new OnCompleteListener<Void>()
                                                                        {
                                                                            @Override
                                                                            public void onComplete(@NonNull Task<Void> task)
                                                                            {
                                                                                Current_State = "friend";
                                                                                AddContactProfileFriendRequest.setEnabled(true);
                                                                                AddContactProfileFriendRequest.setText("Delete Friend");

                                                                                AddContactProfileRefuseRequest.setEnabled(false);
                                                                                AddContactProfileRefuseRequest.setVisibility(View.INVISIBLE);
                                                                            }
                                                                        });
                                                            }
                                                        });
                                            }
                                        }
                                    });
                        }
                    }
                });
    }

    private void CancelFriendRequest()
    {
        FriendRequestRef.child(senderUID).child(receiverUID).removeValue()
                .addOnCompleteListener(new OnCompleteListener<Void>()
                {
                    @Override
                    public void onComplete(@NonNull Task<Void> task)
                    {
                        if (task.isSuccessful())
                        {
                            FriendRequestRef.child(receiverUID).child(senderUID).removeValue()
                                    .addOnCompleteListener(new OnCompleteListener<Void>()
                                    {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task)
                                        {
                                            if (task.isSuccessful())
                                            {
                                                AddContactProfileFriendRequest.setText("Friend Request");
                                                AddContactProfileFriendRequest.setEnabled(true);
                                                Current_State = "unfriend";

                                                AddContactProfileRefuseRequest.setVisibility(View.INVISIBLE);
                                                AddContactProfileRefuseRequest.setEnabled(false);

                                            }
                                        }
                                    });
                        }
                    }
                });
    }

    private void SendFriendRequestToReceiver()
    {
        FriendRequestRef.child(senderUID).child(receiverUID).child("request_type").setValue("sent")
                .addOnCompleteListener(new OnCompleteListener<Void>()
                {
                    @Override
                    public void onComplete(@NonNull Task<Void> task)
                    {
                        if (task.isSuccessful())
                        {
                            FriendRequestRef.child(receiverUID).child(senderUID).child("request_type").setValue("received")
                                    .addOnCompleteListener(new OnCompleteListener<Void>()
                                    {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task)
                                        {
                                            if (task.isSuccessful())
                                            {
                                                AddContactProfileFriendRequest.setText("Cancel The Request");
                                                AddContactProfileFriendRequest.setEnabled(true);
                                                Current_State = "request_sent";
                                            }
                                        }
                                    });
                        }
                    }
                });
    }
}
